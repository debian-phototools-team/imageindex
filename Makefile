######################################################################
# $Id: Makefile.dist,v 1.3 2003/03/31 03:01:24 edwinh Exp $
# $Name: v1_1 $
######################################################################

# Where the script binary should go
PREFIX ?= /usr/local

# Where perl lives
PERLPATH = /usr/bin/perl

######################################################################

all: sed

install: all
	install -m 0755 imageindex.sed $(PREFIX)/bin/imageindex
	install -m 0755 autocaption.sed $(PREFIX)/bin/autocaption
	install -m 0644 imageindex.1 $(PREFIX)/share/man/man1/imageindex.1

sed:
	sed -e 's%/usr/bin/perl%$(PERLPATH)%g' imageindex > imageindex.sed
	sed -e 's%/usr/bin/perl%$(PERLPATH)%g' autocaption > autocaption.sed

clean:
	rm -f imageindex.sed
	rm -f autocaption.sed
