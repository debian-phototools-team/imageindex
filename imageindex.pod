=head1 NAME

imageindex - a digital photo gallery tool

=head1 SYNOPSIS

Imageindex is a digital picture gallery program. It provides automatic
generation of thumbnails and other size views of the images and video files,
and W3C compliant HTML to allow viewing of the thumbnails and images or videos.
It also creates montages of all images in a given directory to be used in
directory entries within the HTML.

=head1 DESCRIPTION

Imageindex has evolved from a simple thumbnail-generation program into a
full-blown gallery application. With it you can create static thumbnails and
"medium" views (good for dial-up web viewers) along with static HTML which
presents the images in chronological order (based upon the date in the Exif
header or date stamp of the file itself). Support has now been added for
animated GIFs and video files (we support whatever file formats your
installation of mplayer supports).

Imageindex creates static rather than dynamic content for many reasons. First
and foremost it is much easier to archive pictures onto CD-ROMs on a periodic
basis when the content is just "there" rather than "trapped" in a database of
some sort. You create your own "database" with your own directories and let
imageindex handle the presentation of the images to the world. Imageindex will
use "montages" (or thumbnails of all your thumbnails crammed together) of
subdirectories if they occur alongside images (see the the sample pages on the
imageindex website - http://www.edwinh.org/imageindex/sample.html).

There are four basic "views" that imageindex creates (enabled by default). The
"index" view shows thumbnails of all images in a directory in a basic table
format (defaults to 3 columns). Within each cell of the table basic information
such as the date/time of the picture are given as well as any comment (or
caption) present in the image. Links to the various sizes of image and other
views for the image are also presented by default.

The second view presented is the "slide show" view. In this view the "medium"
sized image is presented along with information such as date/time, any caption
embedded in the image, etc. Currently for video files, the 'medium' slides only
point to the actual video file processed. Future versions will allow for direct
playback inside the browser (much like YouTube). There are "previous" and
"next" links on each page which let the viewer quickly cycle through each
"medium" image without having to constantly invoke the browser's "back" button.

Captions for video files can be created by creating a file with the same
basename as the original video file but having ".txt" as the extension. The
contents of this file will be used just like captions embedded in formats like
JPG when processing the resultant HTML.

In conjunction with the "slide show" view there is a "frame view". When enabled
a link to the frame view appears at the top of the index view's page. When the
frame view is visited, the browser's pane splits into two portions. On the left
all the thumbnails are lined up close together. On the right hand portion the
same "slide show" pages are loaded. As the user clicks on a thumbnail on the
left, it's "slide" view (including the "medium" image) is displayed in the
right had side of the frame. This creates a very convenient mechanism for
browsing through many images.

Finally a "details" view exists. This details view is much like the index view
as the thumbnails (reduced in size further) are presented in a table format,
but much more information is presented in each cell. This is very useful for
images that come from digital cameras where Exif headers have filled with lots
of neat information.

The HTML output of imageindex can be customized by creating a
".imageindexrc" in your $HOME directory and placing certain variables (see
VARIABLES section) in that file and editing to your taste. One of the variables
controls the output of a cascading style sheet which ultimately directs your
browser how to render the HTML. All color, font, indenting, etc. changes you
wish to make can be done in this style sheet variable.

=head1 VARIABLES

When you create your ".imageindexrc" file in your $HOME directory, you can put
any or all of the following variables in there and tweak as needed. This is
Perl code itself and is subsequently "included" into imageindex as it runs. 

You B<must> end the file by putting a "1;" at the end of it. It's a Perl thing!

The values you see in these examples are the program defaults. If you like the
default value of a particular variable you don't need to include it in your
".imageindexrc" file.

Name of the directory that holds thumbnail images

     $thumbnail_dir = 'thumbnail';

Size of the thumbnail images in the "x" direction (pixels). Note that
imageindex preserves the aspect ratio of an image when it is reduced from its
original size to form a thumbnail image. So, if the "x" dimension is smaller
than the "y" dimension, a thumbnail might have an "x" size smaller than
$default_thumbnail_x. 

     $default_thumbnail_x = 200;

Size of the thumbnail images in the "y" direction (pixels). Again, note that
imageindex preserves the aspect ratio of an image when it is reduced from its
original size to form a thumbnail image. So, if the "y" dimension is smaller
than the "x" dimension, a thumbnail might have an "y" size smaller than
$default_thumbnail_y. 

     $default_thumbnail_y = 200;

If both dimensions of the original are within this much of the thumbnail
dimensions we will skip the thumbnail and just use the original as the
"thumbnail."

     $thumbnail_threshold = 1.0;

Size of the "medium" images in the "x" direction (pixels). Note that
imageindex preserves the aspect ratio of an image when it is reduced from its
original size to form a "medium" image. So, if the "x" dimension is smaller
than the "y" dimension, a "medium" image might have an "x" size smaller than
$med_x. 

     $med_x = 800;

Size of the "medium" images in the "y" direction (pixels). Note that
imageindex preserves the aspect ratio of an image when it is reduced from its
original size to form a "medium" image. So, if the "y" dimension is smaller
than the "x" dimension, a "medium" image might have an "y" size smaller than
$med_y. 

     $med_y = 600;

Name of the directory that holds "medium" images

     $med_dir = 'medium';

If both dimensions of the original are within this much of the "medium"
dimensions we will skip creating the medium-size format and just use the
original. This saves needless creating a "medium" image if it's close in size
to the original already.

     $med_threshold = 1.6;

Automatically recurse into subdirectories? Set to 1 to enable.

     $do_recurse = 0;

Generate "medium" images at all? Set to 1 to enable.

     $do_medium = 1;

Generate the "slide show" and frame view? Set to 1 to enable.

     $do_slide = 1;

Generate the "details" view? Set to 1 to enable.

     $do_captions = 1;

Use/display caption info stored in images? Set to 1 to enable.

     $do_detail = 1;

Process subdirectories as entries in the normal "index" and "details" views?
Set to 1 to enable. If an entire directory hierarchy has been processed with
$do_montage set to 1 (see below), the montage file for a given directory will
be used as the "thumbnail" for a subdirectory.

     $do_dirs = 1;

Create a montage of all the images? When enabled all of the images that are
processed are turned into an NxM montage of very small thumbnails in a tiled
pattern. The resulting image is shrunk to the $default_thumbnail_x x
$default_thumbnail_y dimensions and stored in the $thumbnail_dir directory. The
size of the tiles grows as the number of images in a directory increase, but
can be bounded by variables outlined below. Set to 1 to enable.

     $do_montage = 1;

Map ASCII "smiley" patterns embedded within an image's comment into real
"emoticon" images? When enabled the ASCII smiley faces such as :) and :-), the
winks ;) and ;-), and the frowns :( and :-( are mapped to small PNG images that
display the emotion conveyed. Set to 1 to enable.

     $do_emoticons = 1;

Sort timestamps in reverse order.

     $do_reverse = 1;

Process video files. This relies on a fairly recent version of mplayer being
installed on your system. We've tested with up to 0.99.8. The kind of video
files that are supported are up to the codecs that are compiled and used with
mplayer installation on your system.

     $do_video_files = 1;

Overlay a small icon into one of the corners of the thumbnail and medium views
when processing the first frame of a video file. This gives a "visual cue" that
the file being represented in your browser is a video file and not a still
image. 

     $do_video_thumbnail_icons = 1;

Use the following as a regular expression to identify video files by their
extension. For certain technical reasons it was more feasible to rely upon this
quick and effective method. If files from your camera (or whatever) end in a
different extension, just put that extension here too.

     $video_regexp = '(avi|mov|mpg|mpeg|mjpeg|m1v|m2v|wmv|fli|nuv|vob|ogm|vcd|svcd|mp4|qt)';

If you enable the "visual cue" icons for video files mentioned above, the
following variable determines which corner it is placed. Acceptable values are:
SouthWest, NorthWest, NorthEast, SouthEast (case sensitive!).

     $video_icon_gravity = 'SouthWest';

If you enable the "visual cue" icons for video files mentioned above, there are
two to pick from (currently). Set to 1 (default) for a yellow dot with a 'play'
arrow. Set to 2 for a purplish icon of a video camera. More of these will be
created in further releases.

     $video_icon = 1;

The following three variables control what hyperlinks in the HTML output
"point" to. They can be set to the following:

 index : points to the name reference for an image in the index view
 fullsize : points to the actual image itself
 medium : points to the "medium" version of an image
 thumbnail : points to the thumbnail version of an image
 slide : points to the "slide show" HTML page written for an image
 details : points to the name reference for an image in the details
           view

The folling variable controls what the hyperlink for the thumbnail image in the
index view points to:

     $index_linkto = 'slide';

The folling variable controls what the hyperlink for the thumbnail image in the
details view points to:

     $details_linkto = 'index';

The folling variable controls what the hyperlink for the "medium" image in the
slide view points to:

     $slide_linkto = 'fullsize';

Default number of columns to use in the index and detail views

     $default_columns = 3;

Set the orientation of slide frame - 'horizontal' or 'vertical'. When 'vertical'
the browser pane will split vertically with all the thumbnails towards the
left. When 'horizontal' the browser pane splits horizontally with the
thumbnails arranged in the upper portion

     $frame_orient = 'vertical';

The following two variables can be set to any of the following three values:

  top : put the item in question at the top of the page when rendered
  bottom : put the item in question at the bottom of the page when
           rendered
  none : omit the item from the HTML output

Controls if an image caption (or comment) embedded in the image will be
retrieved and written into the HTML output. By default it is written above the
"medium" image presented in the "slide" view.

     $slide_caption = 'top';

Controls if the date/time of an image is written into the HTML output. By
default it is written below the "medium" image presented in the "slide" view.

     $slide_date = 'bottom';

In the "detail" view, the thumbnail images are shrunk to a size smaller than
the normal thumbnails (to conserve valueable space). This sets the number of
times they are shrunk. By default it is shrunk by a factor of 2.

     $detailshrink = 2;

The thumbnail and "medium" images are written out as JPEG files (even if the
original images were not JPEG's). The following two variables control the
"quality" for generated images. The value can range from 0 to 100 where 100
means "don't lose any quality in favor of file size." 

Adjust the quality of the thumbnails being written out

     $thumb_quality = 50;

Adjust the quality of the "medium" images written out

     $med_quality = 80;

Adjust the minimum number of tiles that will be found in a montage image. If
the number of images in a directory is lower than this value, images will
either be repeated or blanks will be inserted (see $montage_fill).

     $montage_min = 4;

Adjust the maximum number of tiles that will be found in a montage. If the
number of images in a directory is higher than this number, the montage will be
made by "evenly picking" $montage_max images in the directory and only using
those.

     $montage_max = 36;

Adjust the space between montage images (pixels).

     $montage_whitespace = 2;

When there is not a "even" number of images in a directory and a complete NxM
tile montage cannot be formed, images can be used again or empty space can be
used. Set to 'repeat' to re-use images and 'blank' to use empty space.

     $montage_fill = 'blank';

The following variable controls all aspects of how the HTML output is rendered
in standards compliant browsers. The contents of this variable will be written
out into a cascading style sheet and the properties found within it will govern
how the HTML is rendered. All color, font, size, alignment, etc. property
changes can take place. This might require some knowledge of cascading style
sheets. A good primer can be found here:

	    http://www.w3schools.com/css/css_reference.asp

     $stylesheet = '
     body { color: black; background: white; }

     /* Fonts in the title */
     h1.title { font-family: "Comic Sans MS",Helvetica,sans-serif; 
                font-size: 200%; font-weight: bold; text-align: center; }
     h2.daterange { font-family: Arial,Helvetica,sans-serif; 
                    font-size: 125%; text-align: center; }
     h3 { font-family: Arial,Helvetica,sans-serif; font-size: 90%; 
          text-align: center; }

     /* Photo captions & Directory titles */
     div.caption { font-family: Arial,Helvetica,sans-serif; 
                   font-size: 100%; font-weight: bold; margin: 1em; }

     /* Overall fonts on the index and details page */
     div.index { font-family: Arial,Helvetica,sans-serif;
                 font-size: 80%; }
     div.detail { font-family: Arial,Helvetica,sans-serif; 
                  font-size: 80%; }
     div.credits { font-family: Arial,Helvetica,sans-serif;
                   font-size: 80%; text-align: right; margin: 10px }

     /* Table attributes */
     table.index { background: #ffffff; border: none; 
                   border-spacing: 8px; }
     td.index { border: none; padding: 3px }
     table.frame { background: #ffffff; border: none }
     td.frame { border: none; padding: 0px }

     /* Image attributes */
     img.index { border: none; }
     img.slide { border: none; }
     img.frame { border: none; }

     /* Link attributes */
     a:link { color: blue; }
     a:visited { color: green; }
     a:hover { color: red; }
     a:active { color: red; }

     ';


Adjust what is presented in "empty" table cells when there are not an "even"
number of images in a directory.

     $emptycell = "<I>empty</I>";

Control the text of a hyperlink to a parent directory. If you do not desire
that this link be present in the index and "details" views "undef" the variable
(undef $updirtext;)

     $updirtext = "up one directory";

Control the text of a hyperlink to the frame view. If you do not desire
that this link be present in the index and "details" views "undef" the variable
(undef $framelinktext;)

     $framelinktext = "slideshow view (frames)";

Control the text of a hyperlink to the detail view. If you do not desire
that this link be present in the index view "undef" the variable
(undef $detaillinktext;)

     $detaillinktext = "details index";

Control the text of a hyperlink to the index view. If you do not desire
that this link be present in the detail view "undef" the variable
(undef $indexlinktext;)

     $indexlinktext = "main index";

Control the default TITLE string written out in the HTML for a given
directory. This is most usually given on a per-directory basis via the command
line and "remembered" within META data inside the index HTML file itself.

     $default_titletext  = "Image directory";

The following five variables control the TITLE attribute on anchor constructs
in the index and frame views. When TITLE attributes are given they are usually
rendered as "tooltip" bubbles that show text when a cursor hovers and stops
over the active link. We use them here to give a visual cue about the image.
These variables work much like printf(1) strings. The values that can be
interpolated for a given image are:

 %f => replaced with the filename of the image
 %d => replaced with the date/time of the image (or mtime of the file)
 %s => replaced with the size of the file (in Kb)
 %r => replaced with the resolution (XxY) of the original image
 %c => replaced with the image's caption (if stored with one)
 %% => replaced with a literal '%' character

The following codes are interpolated when directories are processed and a
montage of that directory is used as the thumbnail of the subdirectory.

 %n => replaced with number of images in a directory
 %b => replaced with the "begin" date from a directory of images
 %e => replaced with the "end" date from a directory of images
 %t => replaced with the "title" from a directory of images

Other characters (including spaces) are literal. "undef" these in your
".imageindexrc" file if you don't want the TITLE attributes to be written into
the HTML. The "date/time" related constructs are interpolated using the
date/time format variables defined below.

Control the TITLE attributes for hyperlinks to thumbnail images within the
frame view. The default is "<filename> - <date>" for an image

    $framethumbtitle  = "%f - %d";

Control the TITLE attributes for hyperlinks to thumbnail images within the
index view. The default is "<filename> (<size>)" for an image

    $indexthumbtitle  = "%f (%s)";

Control the TITLE attributes for hyperlinks to thumbnail images within the
slide view. The default is "<filename> (<size>)" for an image

    $slidethumbtitle  = "%f (%s)";

Control the TITLE attributes for hyperlinks to thumbnail images within the
detail view. The default is caption (or comment) of an image if one was
embedded within it.

    $detailthumbtitle = "%c";

Control the TITLE attributes for hyperlinks to montage images within the
index view when a subdirectory is being presented. The default is to show how
many images the subdirectory had and the date range that is spanned.

    $montagetitle = "%n images %b through %e";

The following eight variables control how dates and times are formatted when
written into the HTML. Again we're using printf(1)-like variables where codes
are interpolated according to a user's taste.

The definitions of the escape sequences come from the POSIX strftime(3)
definitions. NOT ALL of strftime(3) are supported for obvious reasons. 

 %S is replaced by the second as a decimal number (00-60).
 %M is replaced by the minute as a decimal number (00-59).
 %I is replaced by the hour (12-hour clock) as a decimal number (01-12).
 %H is replaced by the hour (24-hour clock) as a decimal number (00-23).
 %p is replaced by national representation of either "ante meridiem" or
    "post meridiem" as appropriate (currently only U.S. "am" or "pm")
 %R is equivalent to "%H:%M" (in *timeformat variables only).
 %r is equivalent to "%I:%M:%S %p" (in *timeformat variables only).

 %Y is replaced by the year with century as a decimal number.
 %y is replaced by the year without century as a decimal number (00-99).
 %m is replaced by the month as a decimal number (01-12).
 %d is replaced by the day of the month as a decimal number (01-31).
 %F is equivalent to "%Y-%m-%d" (in *dateformat variables only).
 %D is equivalent to "%m/%d/%y" (in *dateformat variables only).
 %% is replaced by a literal "%".

Control the way the date is formed in the frame view

    $framedateformat = "%m/%d/%Y";

Control the way the time is formed in the frame view

    $frametimeformat = "%r";

Control the way the date is formed in the index view

    $indexdateformat = "%m/%d/%Y";

Control the way the time is formed in the index view

    $indextimeformat = "%r";

Control the way the date is formed in the slide view

    $slidedateformat = "%m/%d/%Y";

Control the way the time is formed in the slide view

    $slidetimeformat = "%r";

Control the way the date is formed in the detail view

    $detaildateformat = "%m/%d/%Y";

Control the way the date is formed in the detail view

    $detailtimeformat = "%I:%M %p";

Control what the index view's HTML filename will be

    $indexfile = 'index.html';

Control what the detail view's HTML filename will be

    $detailfile = 'details.html';

Control what the frame view's HTML filename will be

    $framefile = 'frame.html';

Control what the slide view's HTML filename will be

    $slidefile =  'slides.html';

Control the name of the directory where all the "slide view" HTML files will be
deposited (one per image)

    $slide_dir = 'slides';

Control the name of the cascading style sheet written out in each directory

    $stylefile = 'style.css';

Control the name of the montage image if enabled

    $montagefile = 'montage.jpg';

Control the prefix of the emoticon PNG image filenames

    $emoticonprefix = 'ii_';

=head1 EXAMPLES

As an example, suppose you just want to change some date/time format strings. A
complete ".imageindexrc" file in this case would be:

  $framedateformat = "%F";
  $frametimeformat = "%R";

  $indexdateformat = "%F";
  $indextimeformat = "%R";

  $slidedateformat = "%F";
  $slidetimeformat = "%R"; 

  $detaildateformat = "%m/%y";

  1; # don't for get this as the last line in the file!
  
=head1 ACKNOWLEDGMENTS

We would like to thank Larry Wall, creator of Perl for his "swiss army
chainsaw" of a scripting language (as well as all those who have hacked on Perl
throughout the years). We would also like to thank all who have contributed to
ImageMagick and its companion module PerlMagick. Without PerlMagick this
software would be exceedingly less robust. Additionally we would like to thank
the creators of mplayer (and all contributors). Without mplayer the support
introduced for video files would never have come about.

=head1 AUTHORS

 Edwin Huffstutler <edwinh at edwinh dot org>
 John Reynolds     <johnjen at reynoldsnet dot org>



