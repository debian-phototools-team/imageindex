Summary: Photo gallery creation tool
Name: imageindex
%define version 1.1
Version: %{version}
Release: 1
Packager: Edwin Huffstutler <edwinh@computer.org>
Source: %{name}-%{version}.tar.gz
Url: http://www.edwinh.org/imageindex/
License: GPL
Group: Applications/Graphics
BuildRoot: /var/tmp/%{name}-root
BuildArch: noarch
Requires: perl-Image-Info
Requires: perl-IO-String
Requires: ImageMagick-perl

%description
Photo gallery creation tool

%prep
%setup -q

%build
make all PREFIX=$RPM_BUILD_ROOT/usr

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/share/man/man1
make install PREFIX=$RPM_BUILD_ROOT/usr

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/*
%doc %{_mandir}/man1/*
%doc CHANGES README COPYING TODO
